const axios = require("axios");
const fetchWeather = async (address) => {
  try {
    const { data } = await axios.get(
      `http://api.weatherstack.com/current?access_key=75db302316138c44e99b5b81fffaeffb&%20query=${address}`
    );
    return `It is currently ${data.current.temperature}  degress out. It feels like ${data.current.feelslike} degress out.`;
  } catch (e) {
    return "API Failed";
  }
};
module.exports = fetchWeather;
