const path = require("path");
const express = require("express");

const fetchWeather = require("./utlis/weatherInfo");

const app = express();
const port = process.env.PORT || 3000;
const publicDirectoryPath = path.join(__dirname, "../public");
app.use(express.static(publicDirectoryPath));

app.get("/products", (req, res) => {
  if (!req.query.search) {
    return res.send({
      error: "You must provide a search term.",
    });
  }
  res.send({
    products: [],
  });
});

app.get("/weather", (req, res) => {
  if (!req.query.address) {
    return res.send({
      error: "You must provide an address.",
    });
  }
  fetchWeather(req.query.address).then((data) => {
    res.send({
      forecast: data,
    });
  });
});

app.listen(port, () => {
  console.log("The Server is up & running on port" + port);
});
